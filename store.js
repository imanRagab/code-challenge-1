#!/usr/bin/env node
//// require modules
// require file system module
var fs = require('fs');

//// read data file
var dataObj = JSON.parse(fs.readFileSync('data.txt', 'utf8'));


//// get user parameters
var command =  process.argv[2];
var options = process.argv.slice(3);

//// switch case on user command

switch(command) {

    case "add":
        add();
        break;

    case "list":
        list();
        break;

    case "get":
        get();
        break;

    case "remove":
        remove();
        break;

    case "clear":
        clear();
        break;

    default:
        console.log("Inavalid command");
}

//// add command

function add() {

    key = options[0];
    value = options[1];

    if(key && value){
        dataObj[key] = value; 
        console.log("Key added successfully.");
    }
    else{
        console.log("You must enter both key and value");
    }   
}

//// list command

function list() {

    for(var key in dataObj) {
        console.log(key + ": " + dataObj[key]);
    }
}

//// get command

function get() { 

    var inputKey = options[0];

    if(inputKey){ //if user entered key
        for(var key in dataObj) {
            
            if(key == inputKey){ //if key found
                console.log(dataObj[key]);
                return
            }
        }
        console.log("Key doesn't exist!")
    }
    else{
        console.log("You must enter key to get!")
    }
 }

//// remove command

function remove() {

    var inputKey = options[0];

    if(inputKey){ //if user entered key

        for(var key in dataObj) {
            
            if(key == inputKey){ //if key found
                delete dataObj[key];
                console.log("Key removed successfully.");
                return;
            }
        }
        console.log("Key doesn't exist!")
    }

    else{
        console.log("You must enter key to remove!")
    }
}

//// clear command

function clear() {
    dataObj = {};
    console.log("Dictionary cleared successfully.");
}

/// write change to data file

fs.writeFileSync('data.txt', JSON.stringify(dataObj))
